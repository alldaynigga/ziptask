<?php

namespace app\tests;

use PHPUnit\Framework\TestCase;
use app\ReverseInt;

/**
 * Class ReverseIntTest
 * @package app\tests
 */
class ReverseIntTest extends TestCase
{
    /**
     * @dataProvider reverseInt32Provider
     */
    public function testReverseInt32($value, $expected, $message)
    {
        $reverseInt = new ReverseInt();
        $this->assertEquals($expected, $reverseInt->reverse32bitInt($value), $message);
    }

    /**
     * @return array[]
     */
    public function reverseInt32Provider()
    {
        return [
            [987654321, 123456789, 'simple number check fail'],
            [123, 321, 'simple number check fail'],
            [123, 321, 'simple number check fail'],
            [-2147483648, -8463847412, 'min number check fail'],
            [2147483647, 7463847412, 'max number check fail'],
            [0, 0, 'zero check fail'],
            [10, 1, 'zero ending number check fail'],
            [['num' => 987654321], 0, 'array check check fail'],
            [(object) ['test' => true], 0, 'object check fail'],
            [true, 0, 'boolean check fail'],
            [false, 0, 'boolean check fail'],
            [null, 0, 'null check fail'],
            ['123456789', 987654321, 'string with correct int check fail'],
            ['string', 0, 'string with incorrect int check fail'],
            ['12312312string', 0, 'string with incorrect int #2 check fail'],
        ];
    }
}
