<?php

namespace app;

/**
 * Class ReverseInt
 */
class ReverseInt
{

    /**
     * Min 32 bit value
     */
    private const MIN_INT_32 = -2147483648;

    /**
     * Max 32 bit value
     */
    private const MAX_INT_32 = 2147483647;

    /**
     * @param mixed $int
     * @return int
     */
    public function reverse32bitInt($int): int
    {
        if (is_bool($int)) {
            return 0;
        }

        $options = [
            'options' => [
                'min_range' => self::MIN_INT_32,
                'max_range' => self::MAX_INT_32,
                'default' => 0,
            ],
        ];
        $int = filter_var($int, FILTER_VALIDATE_INT, $options);
        if (abs($int) < 10) {
            return $int;
        }
        $reversed = $this->reverseValidatedInt(abs($int));

        return $int >= 0 ? $reversed : $reversed * -1;
    }

    /**
     * @param int $int
     * @return int
     */
    protected function reverseValidatedInt(int $int): int
    {
        $strInt = (string) $int;
        $c = strlen($strInt);
        $n = (int) $c / 2;
        for ($i = 0; $i < $n; $i++) {
            $buffer = $strInt[$i];
            $strInt[$i] = $strInt[$c - $i - 1];
            $strInt[$c - $i - 1] = $buffer;
        }

        return (int) $strInt;
    }

}
