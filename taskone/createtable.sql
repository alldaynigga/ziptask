create table if not exists users
(
    id         bigint unsigned auto_increment primary key,
    name       varchar(255) not null,
    email      varchar(255) not null,
    telephone  int(15)      null,
    password   varchar(255) not null,
    created_at timestamp    null,
    updated_at timestamp    null,
    constraint users_email_unique
        unique (email)
)
    collate = utf8mb4_unicode_ci;

