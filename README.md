# README #

Zip 24 task 

### Installation ###

* Install docker on your machine
* Clone repository:

```
mkdir ziptasknaumov && git clone git@bitbucket.org:alldaynigga/ziptask.git ziptasknaumov && cd ziptasknaumov
```

Run docker composer:

Step inside docker dir

```
cd docker
```

Build and run project

```
docker-compose up -d
```

Install dependencies

```
docker-compose run --rm composer update
```

Enjoy!

### Task #1 ###

[createtable.sql](taskone/createtable.sql)

### Task #2 ###

For start tests run following command from the 'docker' directory:
```
docker-compose run --rm php ./vendor/bin/phpunit --debug --bootstrap index.php app/tests
```

* [Reverse Int class](src/app/ReverseInt.php)
* [Reverse Int test](src/app/tests/ReverseIntTest.php)
